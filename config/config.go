package config

import (
	"log"
	"os"

	"github.com/spf13/viper"
)

type Config struct {
	Bot       BotConfig
	Desend    DesendConfig
	System    SystemConfig
	AdminKeys AdminKeys
}

type DesendConfig struct {
	Url string `json:"url"`
}

type BotConfig struct {
	Token string `json:"url"`
}

type SystemConfig struct {
	MaxGoRoutines int64
}
type AdminKeys struct {
	ApiPublic  string `json:"api_public"`
	ApiPrivate string `json:"api_private"`
}

func LoadConfig() (*viper.Viper, error) {
	v := viper.New()

	if _, ok := os.LookupEnv("LOCAL"); ok {
		v.AddConfigPath("config")
	} else {
		v.AddConfigPath("/app/config")
	}

	v.SetConfigName("config")
	v.SetConfigType("yml")

	err := v.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return v, nil
}

func ParseConfig(v *viper.Viper) (*Config, error) {
	var c Config
	err := v.Unmarshal(&c)
	if err != nil {
		log.Fatalf("unable to decode config into struct, %v", err)
		return nil, err
	}
	return &c, nil
}
