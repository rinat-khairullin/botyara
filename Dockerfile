FROM golang:1.17.2-alpine3.14

WORKDIR /app

RUN ls -la

ENV AE_KEY=""

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .

CMD ["go", "run", "/app/cmd/main.go"]
