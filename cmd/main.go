package main

import (
	"OrderApproverBot/config"
	"OrderApproverBot/pkg/secure"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	tb "gopkg.in/telebot.v3"
)

type Responce struct {
	Success bool   `json:"success"`
	Message string `json:"message,omitempty"`
}

func main() {
	v, err := config.LoadConfig()
	if err != nil {
		log.Fatal("Cannot cload config: ", err.Error())
	}
	cfg, err := config.ParseConfig(v)
	if err != nil {
		log.Fatal("Config parse error: ", err.Error())
	}
	log.Println("Config loaded", cfg)
	// ---- Telegram bot
	pref := tb.Settings{
		Token:  cfg.Bot.Token,
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	}
	kernel, err := tb.NewBot(pref)
	if err != nil {
		log.Fatal("Cannot initialize bot: ", err.Error())
	}
	kernel.Handle("/start", func(c tb.Context) error {
		return c.Send("Привет 👋\n\nДля управления fpoc, введите команды /on или /off", tb.ModeMarkdownV2)
	})
	kernel.Handle("/on", SetONStatus(cfg))
	kernel.Handle("/off", SetOFFStatus(cfg))
	kernel.Handle(tb.OnText, func(c tb.Context) error {
		return c.Delete()
	})

	log.Printf("Telegram bot started")
	kernel.Start()
}

func SetONStatus(cfg *config.Config) tb.HandlerFunc {
	return func(c tb.Context) error {
		req, err := http.NewRequest("POST", cfg.Desend.Url+"/status/change/on", nil)

		req.Header.Set("ApiPublic", cfg.AdminKeys.ApiPublic)
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Signature", secure.CalcSignature(cfg.AdminKeys.ApiPrivate, ""))

		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			return c.Send(err.Error(), tb.ModeMarkdown)
		}
		res := Responce{}
		err = json.NewDecoder(resp.Body).Decode(&res)
		if err != nil {
			fmt.Println(err.Error())
			return err
		}
		if res.Message == "" {
			if res.Success {
				return c.Send("Доступ был открыт", tb.ModeMarkdown)
			} else if !res.Success {
				return c.Send("Доступ уже был открыт", tb.ModeMarkdown)
			}
		}
		return c.Send("Что-то пошло не так, убедитесь, что ввели команду верно :)", tb.ModeMarkdown)
	}
}
func SetOFFStatus(cfg *config.Config) tb.HandlerFunc {
	return func(c tb.Context) error {

		req, err := http.NewRequest("POST", cfg.Desend.Url+"/status/change/off", nil)

		req.Header.Set("ApiPublic", cfg.AdminKeys.ApiPublic)
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Signature", secure.CalcSignature(cfg.AdminKeys.ApiPrivate, ""))

		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			return c.Send(err.Error(), tb.ModeMarkdown)
		}
		res := Responce{}
		err = json.NewDecoder(resp.Body).Decode(&res)
		if err != nil {
			return err
		}
		if res.Message == "" {
			if res.Success {
				return c.Send("Доступ был закрыт", tb.ModeMarkdown)
			} else if !res.Success {
				return c.Send("Доступ уже был закрыт", tb.ModeMarkdown)
			}
		}
		return c.Send("Что-то пошло не так, убедитесь, что ввели команду верно :)", tb.ModeMarkdown)
	}
}
